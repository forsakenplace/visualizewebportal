import React from 'react';
//import Router from 'react-router'
import Container from '@material-ui/core/Container';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import Grid from '@material-ui/core/Grid';
import AppBar  from '@material-ui/core/AppBar';
import Button from  '@material-ui/core/Button';
import Toolbar  from '@material-ui/core/Toolbar';
import Footer from './components/Footer'
import { makeStyles } from '@material-ui/core/styles';
import LocalCafeIcon from '@material-ui/icons/LocalCafeOutlined';
import CustomChatbot from './components/chatbot'

import { MemoryRouter as Router } from 'react-router';
import { useParams} from "react-router";
import { Link as RouterLink, useLocation } from 'react-router-dom';
import Link from '@material-ui/core/Link';
import canadaLogo from './static/media/Canada.svg';
import { useTranslation } from "react-i18next";

import i18n from "i18next";

const useStyles = makeStyles((theme) => ({
  splashBackground:{
    background:'radial-gradient(circle, rgba(10,100,82) 0%, rgba(10,100,82) 35%, rgba(0,0,0,1) 100%)'
  },
  root:{
    flexGrow: 1,
  },
  header: {
    paddingTop: '10em'
  },
  appBar:{
    backgroundColor:'#000000'
  },

  linkStyle:{
    color: '#e33445',
    fontSize:'20px',
    paddingRight:'1em'
  },
  mainContent:{
    paddingTop: '2rem',
    paddingBottom: '10rem'
  }
}));

export default function App(props) {

  let { lang } = useParams();
  let location =useLocation();

  const { t } = useTranslation();
  const classes = useStyles();

  if (i18n.language != lang)
  {
    i18n.changeLanguage(lang);
  }

  const flipLang=t('flipLang');

  const clickEventHandler=(evt)=>{console.log(evt)};

  return (
    <React.Fragment>
       <div className={classes.root}>
      <AppBar component="nav" position="sticky">
      <Toolbar className={classes.appBar}>
        <img src={canadaLogo} />
        <Box style={{width:'70%'}}></Box>
        <Link className={classes.linkStyle} component={RouterLink} to={"/" + lang + "/home"}>
          Home
        </Link>
        <Link className={classes.linkStyle} component={RouterLink} to={"/" + lang + "/rshiny"}>
          R Shiny
        </Link>
        <Link className={classes.linkStyle} component={RouterLink} to={location.pathname.replace("/" + lang, "/" + flipLang)}>{t('languageChange')}</Link>
      </Toolbar>
      </AppBar>
      </div>
      <Container component="header" >
        <Box className={classes.splashBackground}>
            <Box align="center"><LocalCafeIcon color="primary" style={{ fontSize: '16em' }} /></Box>
            <Typography color="primary" component="h1" align="center" style={{fontSize: '4rem',fontWeight: '200'}}>Experimental portal</Typography>
            <Typography color="primary" component="p" align="center" style={{fontSize: '1.5rem',fontWeight: '200'}}>An experiment on Daaas potential features</Typography>
        </Box>
      </Container>
      <Container className={classes.mainContent}>
        <Box>
          {props.children}
        </Box>
      </Container>
      <CustomChatbot eventHandler={clickEventHandler} />
      <Footer />
    </React.Fragment>
  );
}
