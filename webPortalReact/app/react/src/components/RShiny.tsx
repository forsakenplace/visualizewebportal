import React from 'react';
import Typography from '@material-ui/core/Typography';
import { useTranslation } from "react-i18next";
import SurveyForm from './SurveyForm';


export default function RShiny(){
    const { t } = useTranslation();

return(
    <React.Fragment>
        <Typography variant="h1">{t("rshinyTitle")}</Typography>
        <SurveyForm />
    </React.Fragment>
);
}