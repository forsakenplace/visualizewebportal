import React,{useEffect,useState} from 'react';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import TextareaAutosize from '@material-ui/core/TextareaAutosize';
import Button from '@material-ui/core/Button';
import { makeStyles } from '@material-ui/core/styles';

export default function SurveyForm(props)
{
    const useStyles = makeStyles((theme) => ({
        textareaStyle:{
            width:'100%'
        },
      }));

      const onChange=(e)=>{
          let selectedItem=formContent.find(item=>item.id==e.target.id.replace("question_",""));
          selectedItem.data=e.target.value;
        var newArr=[];
          newArr.push(...formContent.filter(x=> x.id != e.target.id.replace("question_","")),selectedItem);
          setFormContent(newArr);
        console.log(newArr);
      }

    const handleSubmit=(evt)=>{
        evt.preventDefault();
        console.log(evt.target);
        props.triggerNextStep();
    }


    const classes = useStyles();

    const [error, setError] = useState(null);
    const [isLoaded, setIsLoaded] = useState(false);
    const [items, setItems] = useState([]);
    const [formContent, setFormContent] = useState([]);

    useEffect(() => {
        fetch("http://localhost:5000/eng/api/getsurveyform")
          .then(res => res.json())
          .then(
            (result) => {
              setIsLoaded(true);
              setItems(result.items);
              var arr = [];

              Object.keys(result.items).forEach(
                  function(key){
                    arr.push({...result.items[key],'data':''})
                  }
              );
              setFormContent(arr);
            },
            (error) => {
              setIsLoaded(true);
              setError(error);
            }
          )
      }, [])

      if (error) {
        return <div>Error : {error.message}</div>;
      } else if (!isLoaded) {
        return <div>Loading...</div>;
      } else {
        return (
            <form onSubmit={handleSubmit}>
                {items.map(item => (
                     <div key={"k_" + item.id}>   
                        <InputLabel htmlFor={"question_" + item.id}>{item.question_text}</InputLabel>
                        <br/>
                        <TextareaAutosize id={"question_" + item.id} name={"question_" + item.id} rowsMin={3} className={classes.textareaStyle} onChange={onChange} />
                    </div>
                  ))}
                <Button type="submit">Submit</Button>
            </form>);
      }
}