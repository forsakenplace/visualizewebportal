import React,{useState, useEffect} from 'react';
import ChatBot from "react-simple-chatbot";
import SurveyForm from './SurveyForm';

function CustomChatbot(props) {
  const [error, setError] = useState(null);
  const [isLoaded, setIsLoaded] = useState(false);
  const [items, setItems] = useState([]);

    const config = {
        width: "400px",
        height: "500px",
        floating: true
      };

      const initialSteps=[
        {
          id: "Ask Name",
          message: "Hi! What is your name?",
          trigger: "Waiting user input for name"
       },
       {
          id: "Waiting user input for name",
          user: true,
          trigger: "greet"
       },
       {
        id: "greet",
        message: "Hi {previousValue} !",
        trigger: "choose_start"
        },
        {
          id: "choose_start",
          options: [
            { 
              value: "chatbot",
              label: "Chatbot",
              trigger: "1"
            },
            { 
              value: "survey",
              label: "SurveyForm",
              trigger: "survey_start"
            } 
          ]
        },
        {
          id: "option_beginning",
          options: [
            { 
              value: "reset",
              label: "Bring me back to the beginning",
              trigger: "choose_start"
            } 
          ]
        },
        {
          id: "survey_start",
          component:<SurveyForm />,
          trigger:"choose_start",
          waitAction:true
        }
      ];

      /*
,
        {
          id: "Adding Mushroom in Pizza",
          options: [
                     {
                       value: true,
                       label: "Yes",
                       trigger: () => {
                          props.eventHandler("mushroom");
                          return "Asking for Corn in Pizza"  
                        }
                     },
                     { 
                       value: "false",
                       label: "No",
                       trigger: "Asking for Corn in Pizza"
                     } 
                   ]
       },
      */
      useEffect(() => {
        fetch("http://localhost:5005/en/questionreact")
          .then(res => res.json())
          .then(
            (result) => {
              var finalConversation=initialSteps.concat(result.items);
              setItems(finalConversation);
              setIsLoaded(true);
            },
            // Remarque : il faut gérer les erreurs ici plutôt que dans
            // un bloc catch() afin que nous n’avalions pas les exceptions
            // dues à de véritables bugs dans les composants.
            (error) => {
              setIsLoaded(true);
              setError(error);
            }
          )
      }, [])

      /*
      const steps = [
        {
           id: "Greet",
           message: "Hello, Welcome to our shop",
           trigger: "Ask Name"
        },
        {
           id: "Ask Name",
           message: "Please type your name?",
           trigger: "Waiting user input for name"
        },
        {
           id: "Waiting user input for name",
           user: true,
           trigger: "Asking options to eat"
        },
        {
           id: "Asking options to eat",
           message: "Hi {previousValue}, Please click on what you want to eat!",
           trigger: "Displaying options to eat"
        },
        {
           id: "Displaying options to eat",
           options: [
                      {
                        value: "pizza",
                        label: "Pizza",
                        trigger: "Asking for Tomatoes in Pizza"
                      },
                      { 
                        value: "burger",
                        label: "Burger",
                        trigger: "Burger Not available"
                      } 
                    ]
        },
        {
           id: "Burger Not available",
           message: "Sorry, We don't have burger available at the moment. Would you like to try our pizza?",
           trigger: "Asking for pizza after burger"
        },
        {
           id: "Asking for pizza after burger",
           options: [
                      {
                        value: true,
                        label: "Yes",
                        trigger: "Asking for Tomatoes in Pizza"
                      },
                      { 
                        value: "false",
                        label: "No",
                        trigger: "Done"
                      } 
                    ]
        },
        {
           id: "Asking for Tomatoes in Pizza",
           message: "Would you like to have tomatoes in your pizza",
           trigger: "Adding Tomatoes in Pizza"
        },
        {
           id: "Adding Tomatoes in Pizza",
           options: [
                      {
                        value: true,
                        label: "Yes",
                        trigger: () => {
                           props.eventHandler("tomato");
                           return "Asking for Mushroom in Pizza"  
                         }
                      },
                      { 
                        value: "false",
                        label: "No",
                        trigger: "Asking for Mushroom in Pizza"
                      } 
                    ]
        },
        
        {
           id: "Asking for Mushroom in Pizza",
           message: "Would you like to have mushroom in your pizza",
           trigger: "Adding Mushroom in Pizza"
        },
 
        {
           id: "Adding Mushroom in Pizza",
           options: [
                      {
                        value: true,
                        label: "Yes",
                        trigger: () => {
                           props.eventHandler("mushroom");
                           return "Asking for Corn in Pizza"  
                         }
                      },
                      { 
                        value: "false",
                        label: "No",
                        trigger: "Asking for Corn in Pizza"
                      } 
                    ]
        },
        {
           id: "Asking for Corn in Pizza",
           message: "Would you like to have corn in your pizza",
           trigger: "Adding Corn in Pizza"
        },
 
        {
           id: "Adding Corn in Pizza",
           options: [
                      {
                        value: true,
                        label: "Yes",
                        trigger: () => {
                           props.eventHandler("corn");
                           return "Asking for Veggies in Pizza"  
                         }
                      },
                      { 
                        value: "false",
                        label: "No",
                        trigger: "Asking for Veggies in Pizza"
                      } 
                    ]
        },
         
        {
           id: "Asking for Veggies in Pizza",
           message: "Would you like to have veggies in your pizza",
           trigger: "Adding Veggies in Pizza"
        },
 
        {
           id: "Adding Veggies in Pizza",
           options: [
                      {
                        value: true,
                        label: "Yes",
                        trigger: () => {
                           props.eventHandler("veggie");
                           return "Done"  
                         }
                      },
                      { 
                        value: "false",
                        label: "No",
                        trigger: "Done"
                      } 
                    ]
        },
        {
            id: "Done",
            message: "Have a great day !!",
            end: true
        }
        
];*/

      if (error) {
        return <div>Error : {error.message}</div>;
      } else if (!isLoaded) {
        return <div>Loading...</div>;
      } else {
        console.log(items);
        return (
          <ChatBot steps={items} {...config} />
        );
      }

   
  }

  export default CustomChatbot
  