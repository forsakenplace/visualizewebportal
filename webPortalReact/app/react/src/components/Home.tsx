import React,{useState} from 'react';
import Grid from '@material-ui/core/Grid';
import ListItem from './ListItem';
import ContentTile from './ContentTile';
import StorageOutlinedIcon from '@material-ui/icons/StorageOutlined';
import MemoryOutlinedIcon from '@material-ui/icons/MemoryOutlined';
import { makeStyles } from '@material-ui/core/styles';
import AddDbEntry from './database/AddDbEntry';
import ManageDbEntries from './database/ManageDbEntries';

const useStyles = makeStyles((theme) => ({
    secondaryContentIcon:{
      fontSize:'5em',
      color:'red'
    },
    contentIcon:{
      fontSize:'5em',
      color:'#FFFFFF'
    },
    linkStyle:{
      "&:hover": {
        textDecoration: 'none',
        backgroundColor:'#151515'
        },
        display:'block',
        textDecoration:'none'
      }
  }));

export default function Home(){
    const classes = useStyles();

    const [openAddEntry, setOpenAddEntry] = useState(false);
    const [openManageRec, setOpenManageRecords] = useState(false);

    const handleClickManageEntry=(evt)=>{
      evt.preventDefault();
      setOpenManageRecords(true);
    };

    const handleClickCloseManageEntry=()=>{
      setOpenManageRecords(false);
    };

    const handleClickCloseAddEntry=()=>{
      setOpenAddEntry(false);
    };

    const handleClickAddEntry=(evt)=>{
      evt.preventDefault();
      setOpenAddEntry(true);
    };

    return (<Grid container justify="center" spacing={2}>
    <Grid item xs={6}>
      <ContentTile title="Database experiment">
          <StorageOutlinedIcon className={classes.contentIcon} />
          <React.Fragment>
            <Grid item>
            <a href="/manageentries" onClick={handleClickManageEntry} className={classes.linkStyle}>
              <ManageDbEntries open={openManageRec} onClose={handleClickCloseManageEntry} />
            <ListItem description="Manage all records in database" title="Manage records">
              <MemoryOutlinedIcon className={classes.secondaryContentIcon} />
            </ListItem>
            </a>
            </Grid>
            <Grid item>
            <a href="/addentry" onClick={handleClickAddEntry} className={classes.linkStyle}>
            <AddDbEntry open={openAddEntry} onClose={handleClickCloseAddEntry}/>
            <ListItem description="Add a new entry in a test table" title="Add new entry">
              <MemoryOutlinedIcon className={classes.secondaryContentIcon} />
            </ListItem>
            </a>
            </Grid>
          </React.Fragment>                  
      </ContentTile>
    </Grid>
    <Grid item xs={6}>
        <ContentTile title="Other experiments">
          <StorageOutlinedIcon className={classes.contentIcon} />
          <React.Fragment>
            <Grid item>
              <ListItem description="A simple API call" title="API test call">
                <MemoryOutlinedIcon className={classes.secondaryContentIcon} />
              </ListItem>
            </Grid>
          </React.Fragment>                  
      </ContentTile>
    </Grid>
</Grid>)
}
