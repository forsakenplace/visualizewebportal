import React from 'react';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import ListItem from './ListItem';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';

const useStyles = makeStyles((theme) => ({
    contentTile:{
        backgroundColor: 'rgba(21,21,21,.72)',
        backgroundImage: 'radial-gradient(circle at center top,rgba(174,174,174,.08) 10%,rgba(174,174,174,0))',
        borderRadius: '2px',
        padding: '2rem',
        textAlign:'center'
      }
    }));
    


export default function ContentTile(props){
    const classes = useStyles();
    return(<Paper color="primary" className={classes.contentTile}>
        {props.children[0]}
    <Typography variant="h2" component="h2" color="primary">{props.title}</Typography>
    <Grid container justify="center" direction="column" spacing={1} style={{paddingLeft:'7rem',marginTop:'1rem'}}>      
            {props.children[1]}
    </Grid>
  </Paper>)
}
