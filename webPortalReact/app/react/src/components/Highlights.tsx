import React from 'react';
import Typography from '@material-ui/core/Typography';
import { useTranslation } from "react-i18next";

export default function Highlights(){
    const { t } = useTranslation();
return(
    <React.Fragment>
        <Typography variant="h1">{t('highlightsTitle')}</Typography>
    </React.Fragment>
);
}