import React from 'react';
import Grid from '@material-ui/core/Grid';
import sigLogo from '../static/media/sig-wht-eng.svg';
import canadaLogo from '../static/media/Canada.svg';

export default function Footer(){
    return(<Grid container justify="center">
        <Grid item xs={6} style={{textAlign: 'center',paddingLeft:'15em'}}>
        
        <img src={sigLogo} />

        </Grid>
        <Grid item xs={6} style={{textAlign: 'center',paddingRight:'15em'}}>
        <img src={canadaLogo} />
        </Grid>
    </Grid>)    
    }
