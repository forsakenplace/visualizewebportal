import React from 'react';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import { Link as RouterLink } from 'react-router-dom';
import Link from '@material-ui/core/Link';
import { makeStyles } from '@material-ui/core/styles';

export default function ListItem(props){

    return(<Grid container direction="row">
    <Grid item xs={2}>{props.children}</Grid>
    <Grid item xs={7}>
      <Box style={{textAlign:'left'}}><Typography component="span" color="primary" style={{fontSize:'1.5rem'}}>{props.title}</Typography></Box>
<Box style={{textAlign:'left'}}><Typography component="span" color="secondary">{props.description}</Typography></Box>
    </Grid>
  </Grid>)
}
