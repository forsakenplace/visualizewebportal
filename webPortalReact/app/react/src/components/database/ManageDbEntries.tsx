import React from 'react';
import Modal from '@material-ui/core/Modal';
import MaterialTable from 'material-table';
import { makeStyles } from '@material-ui/core/styles';
import {useState, useEffect} from 'react';

function rand() {
  return Math.round(Math.random() * 20) - 10;
}

function getModalStyle() {
  const top = 50 + rand();
  const left = 50 + rand();

  return {
    top: `${top}%`,
    left: `${left}%`,
    transform: `translate(-${top}%, -${left}%)`,
  };
}

const useStyles = makeStyles((theme) => ({
  paper: {
    position: 'absolute',
    width: '90%',
    backgroundColor: theme.palette.background.paper,
    border: '2px solid #000',
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
  },
}));

export default function ManageDbEntries(props){

  const classes = useStyles();
  const [error, setError] = useState(null);

  const [modalStyle] = useState(getModalStyle);

  const [state, setState] = useState({
        columns: [
          { title: 'Text', field: 'text' }
        ],
        data:[]
      });

    //not sure having a method inside a component is a good practice...to confirm
    function reloadData()
    {
      fetch("http://localhost:5002/action/viewentries")
      .then(res => res.json())
      .then(
        (result) => {
          setState({columns: state.columns,
            data: result.items});
        },
        // Remarque : il faut gérer les erreurs ici plutôt que dans
        // un bloc catch() afin que nous n’avalions pas les exceptions
        // dues à de véritables bugs dans les composants.
        (error) => {
          //setIsLoaded(true);
          setError(error);
        }
      )
    }

      useEffect(() => {

        reloadData();
          
      }, [props.open])

    return (<Modal open={props.open} onClose={props.onClose}>
       <div style={modalStyle} className={classes.paper}>
<MaterialTable
      title="Manage records"
      columns={state.columns}
      data={state.data}
      /*data={query =>
        new Promise((resolve, reject) => {
          let url = 'http://localhost:5002/action/viewentries'
          fetch(url)
            .then(response => response.json())
            .then(result => {
              resolve({
                data: result.items
                //page: result.page - 1,
                //totalCount: result.total,
              })
            })
        })
      }*/
      editable={{
        onRowUpdate: (newData, oldData) =>
          new Promise((resolve) => {
            setTimeout(() => {
             

              if (oldData) {
                
                fetch('http://localhost:5002/action/updateentry', {
                  method: 'POST',
                  headers: {
                      Accept: 'application/json',
                      'Content-Type': 'application/json',
                  },
                  body: JSON.stringify({
                      textex: newData.text,
                      id: newData.id
                  }),
                  })
                  .then(res => res.json())
                  .then(
                    (result) => {
                      if (result.success!="true")
                      {
                          setError("An error happened.");
                      }
                      else
                      {
                          reloadData();
                          //setReload(true);
                          /*setState((prevState) => {
                            const data = [...prevState.data];
                            data[data.indexOf(oldData)] = newData;
                            return { ...prevState, data };
                          });*/
                      }
                    },
                    (error) => {
                      setError(error);
                    }
                  );

              }
              resolve();
            }, 600);
          }),
        onRowDelete: (oldData) =>
          new Promise((resolve) => {
            setTimeout(() => {

              fetch('http://localhost:5002/action/deleteentry', {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    id: oldData.id
                }),
                })
                .then(res => res.json())
                .then(
                  (result) => {
                    if (result.success!="true")
                    {
                        setError("An error happened.");
                    }
                    else
                    {
                        reloadData();
                        //setReload(true);
                        /*setState((prevState) => {
                          const data = [...prevState.data];
                          data[data.indexOf(oldData)] = newData;
                          return { ...prevState, data };
                        });*/
                    }
                  },
                  (error) => {
                    setError(error);
                  }
                );
                resolve();
              /*setState((prevState) => {
                const data = [...prevState.data];
                data.splice(data.indexOf(oldData), 1);
                return { ...prevState, data };
              });*/
            }, 600);
          }),
      }}
    /></div>
    </Modal>

    )
}