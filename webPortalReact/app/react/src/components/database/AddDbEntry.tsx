import React,{useState} from 'react';
import Grid from '@material-ui/core/Grid';
import { Typography } from '@material-ui/core';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import Input from '@material-ui/core/Input';
import FormHelperText from '@material-ui/core/FormHelperText';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Box from '@material-ui/core/Box';

import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import TextField from '@material-ui/core/TextField';

const useStyles = makeStyles((theme) => ({
    root: {
     color:'#FFFFFF',
     background:'#000000',
    },
    input:{
        backgroundColor:'#FFFFFFFF'
    }
  }));

  function ErrorBox(props) {
    const errorMsg = props.errorMsg;
    if (errorMsg) {
      return (<Box><Typography>{errorMsg}</Typography></Box>)
    }
    return null;
  }

export default function AddDbEntry(props){
    const classes = useStyles();

    const [error, setError] = useState(null);
    const [text, setText] = useState("");

    const handleSubmit = () => {
        
      if (text=="")
      {
        setError("Fill out the field!");
        return;
      }

        fetch('http://localhost:5002/action/addentry', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                textex: text
            }),
            })
            .then(res => res.json())
            .then(
              (result) => {
                if (result.success!="true")
                {
                    setError("An error happened.");
                }
                else
                {
                    //clear text field
                    setText("");
                    props.onClose();
                  //redirect to manage records
                }
              },
              // Remarque : il faut gérer les erreurs ici plutôt que dans
              // un bloc catch() afin que nous n’avalions pas les exceptions
              // dues à de véritables bugs dans les composants.
              (error) => {
              
                setError(error);
              }
            );

    }

    return (
      <Dialog open={props.open} onClose={props.onClose} className={classes.root} aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title">Add a new entry</DialogTitle>
        <DialogContent>
          <ErrorBox errorMsg={error} />
          <DialogContentText>
            To feel great again fill the box below.
          </DialogContentText>
          <TextField
            autoFocus
            margin="dense"
            id="textex"
            label="Text field"
            type="text"
            fullWidth
            onChange={e => setText(e.target.value)} />
        </DialogContent>
        <DialogActions>
          <Button onClick={props.onClose}>
            Cancel
          </Button>
          <Button onClick={handleSubmit}>
            Add
          </Button>
        </DialogActions>
      </Dialog>
    )
}