import React from 'react';
import ReactDOM from 'react-dom';
import CssBaseline from '@material-ui/core/CssBaseline';
import { ThemeProvider } from '@material-ui/core/styles';
import App from './App';
import theme from './theme';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect
} from "react-router-dom";
import Highlights from './components/Highlights'
import RShiny from './components/RShiny'
import i18n from "i18next";
import { initReactI18next } from "react-i18next";

import translationEN from './locales/en/translation.json';
import translationFR from './locales/fr/translation.json';

// the translations
const resources = {
  en: {
    translation: translationEN
  },
  fr:{
    translation: translationFR
  }
};

i18n
    .use(initReactI18next)
    .init({
        resources,
        lng: "en",
        fallbackLng: "en"
    });

ReactDOM.render(
  <Router>
    <ThemeProvider theme={theme}>
      {/* CssBaseline kickstart an elegant, consistent, and simple baseline to build upon. */
      /* TODO must find a way to extract App component */
      }
      <CssBaseline />
        <Switch>
            <Route path="/:lang/rshiny">
              <App><RShiny/></App>
            </Route>
            <Route path="/:lang/home">
              <App><Highlights/></App>
            </Route>
            <Route exact path="/">
                <Redirect from='/' to='/en/home' /> 
            </Route>
          </Switch>
    </ThemeProvider>
  </Router>,
  document.querySelector('#root'),
);
