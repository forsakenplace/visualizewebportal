import { red } from '@material-ui/core/colors';
import { createMuiTheme } from '@material-ui/core/styles';

// A custom theme for this app
const theme = createMuiTheme({
  palette: {
    primary: {
      main: '#FFFFFF',
      dark: '#000000'
    },
    secondary: {
      main: '#2caea5',
    },
    error: {
      main: red.A400,
    },
    background: {
      default: '#000000',
    }
  },
  typography:{
    fontFamily:['Fira Sans','sans-serif'],
    h2: {
      fontSize: "2rem"
    },
    h1:{
      fontSize: "3rem",
      color: "#FFFFFF"
    }
  }
});

export default theme;
