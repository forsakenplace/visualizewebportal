class Config:
    SQLALCHEMY_DATABASE_URI = "postgresql://testuser:testpassword@db/testdb"
    SQLALCHEMY_TRACK_MODIFICATIONS = "False"
    SECRET_KEY = "super_Secret_value"