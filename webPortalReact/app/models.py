from app import db

class Entry(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    text = db.Column(db.String(100))

    def __repr__(self):
        return "Entry({0}, {1})".format(self.id, self.text)

class ShinyReport(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(100))
    location = db.Column(db.String(100))

    def __repr__(self):
        return "ShinyReport({0}, {1}, {2})".format(self.id, self.title, self.location)

class Highlight(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(50))
    image = db.Column(db.String(50), default="default.jpg")
    description = db.Column(db.String(100))
    link = db.Column(db.String(50))

    def __repr__(self):
        return "Highlight({0}, {1}, {2}, {3}, {4})".format(self.id, self.title, self.image, self.description, self.link)
