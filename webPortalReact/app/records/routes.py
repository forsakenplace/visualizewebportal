from flask import Blueprint, render_template, request, jsonify
from app.models import Entry
from app import db

records = Blueprint("records", __name__)

@records.route("/action/addentry", methods=['POST'])
def actionaddentry():
    response={"success": "true"}
    try:
        content = request.json
        entry = Entry(text= content['textex'])
        db.session.add(entry)
        db.session.commit()
    except:
        response={"success": "false"}
        
    return jsonify(response)

@records.route("/action/updateentry", methods=['POST'])
def actionupdateentry():

    content = request.json
    entry = Entry.query.get_or_404(int(content['id']))
    entry.text = content['textex']
    db.session.commit()
    response={"success": "true"}
    return jsonify(response)
    
@records.route("/action/viewentries")
def actionviewentries():

    #gets the data from the table
    entries = Entry.query.order_by(Entry.text)

    itemList = [{'id':entry.id,'text':entry.text} for entry in entries]

    return jsonify(items=itemList)

@records.route("/action/deleteentry", methods=["POST"])
def delete_post():

    content = request.json
    entry = Entry.query.get_or_404(int(content['id']))
    
    db.session.delete(entry)
    db.session.commit()
    response={"success": "true"}
    return jsonify(response)