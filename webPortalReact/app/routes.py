from flask import Flask,render_template, request, send_from_directory,jsonify,current_app as app


@app.route("/getData", methods=["GET"])
def serveJSON():
    itemList=[
        { "id": 1, "name": "Peashooter",  "price": "$5" },
        { "id": 2, "name": "Spread", "price": "$3" },
        { "id": 3, "name": "Chaser", "price": "$6" }
    ]
    return jsonify(items=itemList)
