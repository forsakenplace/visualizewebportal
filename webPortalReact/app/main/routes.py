from flask import Blueprint, render_template, send_from_directory
from app import db
import requests


main = Blueprint("main", __name__)

@main.route("/")
def home():
    return render_template("index.html")

#TODO this is bad...need to create generic route that will catch all url of applications
@main.route("/en/home")
@main.route("/fr/home")
@main.route("/en/rshiny")
@main.route("/fr/rshiny")
def otherroutes():
    return render_template("index.html")

@main.route("/<path:path>", methods=["GET"])
def serveReactContent(path):
    return send_from_directory("static",path)
