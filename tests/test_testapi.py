import pytest
import requests
from requests.exceptions import ConnectionError


def is_responsive(url):
    try:
        response = requests.get(url)
        if response.status_code==200:
            return True
    except ConnectionError:
        return False


@pytest.fixture(scope="session")
def http_service(docker_ip, docker_services):
    port = docker_services.port_for("testapi", 5001)
    url = "http://{}:{}".format(docker_ip, port)
    docker_services.wait_until_responsive(timeout=15.0, pause=0.1, check=lambda: is_responsive(url))
    return url


def test_status_code(http_service):
    response = requests.get(http_service)

    assert response.status_code == 200