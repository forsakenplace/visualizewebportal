from setuptools import setup, find_packages

setup(
    name="webPortal",
    packages=find_packages(),
    install_requires=[
        'flask'
    ],
    entry_points={
        'console_scripts':[
            "webPortal=app.cli:cli"
        ]
    }
)