��    :      �  O   �      �     �     �          %     6  
   D     O  )   ^     �  	   �     �     �  D   �  	     	          	   *     4     D  
   c     n     s     y  !   �     �     �  
   �     �     �     �     �             	     
      $   +     P     `  	   g     q     x  <        �     �  )   �                    )     7     L     Y     q     �  &   �     �     �  k  �     &
     .
     K
     h
     �
     �
     �
  7   �
          %     5     Q  Z   ]  
   �  
   �     �     �  #   �  +     
   A     L     S     Y  %   s     �     �     �     �  
   �     �     �     �       
     
   %  '   0     X     i  	   p     z     �  M   �  	   �     �  2   �     ,     2     L     g     �     �     �     �  	   �  -   �                          %      2   +              6      ,   #   	   .   !              7          5                4           (             )                    9       :   "       $                       -   0             
              1             3      *   '         &       /   8          Add Add a new Highlight Add a new highlight Add a new survey Add new entry Add record Admin features An experiment on Daaas potential features Chatbot Graph Dashboard Database example Description Display r-shiny report in iframe. Click on a row to view the report. Error 404 Error 500 Experimental portal Français Have a good day Hi! What would you like to do? Highlights Home Image Internal Server Error Learn more about the DaaaS system Let's start over Link Loading... Log out Login Manage records Manage responses Manage surveys Modify Operation Operations Provide my feedback about the Portal R-shiny feature RShiny Read more Remove Return Sorry, I seem to be asleep right now. Please try again later Submit Thank you for your time! The requested resource could not be found Title Update Entry Update question Update survey Update the Highlight View records View the chatbot script Weekly highlights Welcome Your browser does not support iframes. id text Project-Id-Version: PROJECT VERSION
Report-Msgid-Bugs-To: EMAIL@ADDRESS
PO-Revision-Date: 2020-06-10 10:00-0400
Last-Translator: 
Language: fr_FR
Language-Team: fr_FR <LL@li.org>
Plural-Forms: nplurals=2; plural=(n > 1);
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 2.8.0
X-Generator: Poedit 2.2.4
 Ajouter Ajouter un nouveau highlight Ajouter un nouveau highlight Ajouter une nouvelle enquête Ajouter une nouvelle donnée Ajouter une nouvelle donnée Fonctions d'administration Une expérience sur les fonctions potentielles du DaaaS Graphe du chatbot Tableau de bord Example de base de données Description Démontre un rapport R-Shiny dans un iframe. Cliquez sur une rangée pour voir le rapport. Erreur 404 Erreur 500 Portail d'expérimentation English On vous souhaite une bonne journée Bonjour! Qu'est-ce que vous aimeriez faire? Highlights Maison Image Erreur de serveur interne Apprendre à propos du système DaaaS Recommençons Lien Chargement... Déconnection Connection Gérer les données Gérer les réponses Gérer les enquêtes Modifier Opération Operations Donner mon opinion à propos du Portail Fonction R-shiny RShiny Lire plus Retirer Retour Désoler mais on dirait que je dors en ce moment. Veuilliez essayer plus tard Soumettre Merci pour votre temps! La resource demandé ne pouvait pas être trouvée Titre Mettre à jour la donnée Mettre à jour la question Mettre à jour l'enquête Mettre à jour le Highlight Voir les données Voir le scripte du chatbot Highlights hebdomadaires Bienvenue Votre navigateur ne supporte pas les iframes. id texte 