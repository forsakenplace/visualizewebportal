from flask import Blueprint, render_template, send_from_directory, g
from flask_babel import gettext
from app import db
import requests
from app.models import ShinyReport,Highlight,User,ChatbotFeedbackSurvey,ChatbotFeedbackQuestion

main = Blueprint("main", __name__)

@main.route("/translate")
def translate():
    lang = g.lang_code
    if lang == "eng":
        return "fra"
    else:
        return "eng"

@main.route("/")
@main.route("/home")
def home():
    #TODO remove code duplication from highlight action :(
    highlights = Highlight.query.all()
    i = 0
    output = []
    templist = []
    for highlight in highlights:
        templist.append(highlight)
        i = i+1
        if i%3 == 0:
            output.append(templist)
            templist = []
    
    if templist!= []:
        output.append(templist)

    return render_template("intro.html", headerText=gettext("Weekly highlights"),highlights=highlights, output=output)


@main.route("/<path:path>", methods=["GET"])
def serve_static_content(path):
    return send_from_directory("static",path)


@main.route("/test_the_api")
def test_the_api():
    response = requests.get("http://test_api:5001/")

    return str(response.text)