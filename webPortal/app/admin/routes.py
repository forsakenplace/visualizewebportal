from sqlalchemy.sql import func
from flask import Blueprint, render_template, redirect, url_for,request,flash
from app import db
from app.admin.forms import LoginForm
from app.models import User
from flask_login import current_user, login_user,login_required,logout_user


admin = Blueprint("admin", __name__)


@admin.route("/login", methods=["GET", "POST"])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('admin.dashboard'))
    form = LoginForm()

    if form.validate_on_submit():
     
        user = User.query.filter(
            func.lower(User.username) == func.lower(form.username.data)
        ).first()
        if user is None or not user.check_password(form.password.data):
            form = LoginForm()
            flash('Invalid login. Please try again.','error')
        else:
            login_user(user)
            return redirect(url_for('admin.dashboard'))


    return render_template("admin/login.html",form=form)

@admin.route('/dashboard/chatbot')
@login_required
def dashboardchatbot():
    return render_template('admin/dashboard.html')

@admin.route('/dashboard')
@login_required
def dashboard():
    return render_template('admin/dashboard.html')

@admin.route('/logout')
@login_required
def logout():
    logout_user()
    flash('You are now logged out. See you again!')
    form = LoginForm()
    return render_template('admin/login.html',form=form)