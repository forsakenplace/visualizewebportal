class Config:
    SQLALCHEMY_DATABASE_URI = "postgresql://testuser:testpassword@db/testdb"
    SQLALCHEMY_TRACK_MODIFICATIONS = "False"
    SECRET_KEY = "super_Secret_value"
    LANGUAGES = {
        "eng":"en_CA",
        "fra":"fr_CA"
    }
    BABEL_DEFAULT_LOCALE = 'eng'
    BABEL_TRANSLATION_DIRECTORIES = "/app/translations"