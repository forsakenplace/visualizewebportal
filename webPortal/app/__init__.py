import flask_login

from flask import Flask, request, session, g, redirect, abort, send_from_directory, render_template
from flask_sqlalchemy import SQLAlchemy
from app.config import Config
from flask_login import LoginManager
from flask_babel import Babel
import click

db = SQLAlchemy()
babel = Babel()

@babel.localeselector
def define_locale():
    lang = g.get("lang_code")
    if lang in Config.LANGUAGES.keys():
        return Config.LANGUAGES[lang]
    else:
        return Config.LANGUAGES[Config.BABEL_DEFAULT_LOCALE]

def create_app(config_class=Config):
    app = Flask(__name__)
    babel.init_app(app)
    login_manager = flask_login.LoginManager()
    login_manager.login_view = "admin.login"
    app.config.from_object(config_class)
    db.init_app(app)
    login_manager.init_app(app)

    from app.chatbot.routes import chatbot
    from app.highlights.routes import highlights
    from app.main.routes import main
    from app.records.routes import records
    from app.shiny.routes import shiny
    from app.errors.handlers import errors
    from app.admin.routes import admin

    app.register_blueprint(main, url_prefix="/<lang_code>")
    app.register_blueprint(chatbot, url_prefix="/<lang_code>")
    app.register_blueprint(highlights, url_prefix="/<lang_code>")
    app.register_blueprint(records, url_prefix="/<lang_code>")
    app.register_blueprint(shiny, url_prefix="/<lang_code>")
    app.register_blueprint(errors, url_prefix="/<lang_code>")
    app.register_blueprint(admin, url_prefix="/<lang_code>")

    @app.url_defaults
    def set_lang_code(endpoint, values):
        if "lang_code" in values or not g.get("lang_code", None):
            return
        if app.url_map.is_endpoint_expecting(endpoint, "lang_code"):
            values["lang_code"] = g.lang_code

    @app.url_value_preprocessor
    def get_lang_code(endpoint, values):
        default_val = request.accept_languages.best_match(Config.LANGUAGES.keys(), Config.BABEL_DEFAULT_LOCALE)
        if values:
            g.lang_code = values.pop("lang_code", request.accept_languages.best_match(Config.LANGUAGES.keys()))
        else:
            g.lang_code = default_val

    @app.before_request
    def ensure_lang_code():
        lang_code = g.get("lang_code", None)
        if lang_code and lang_code not in Config.LANGUAGES.keys():
            return abort(404)

    @app.route("/")
    def redir():
        return redirect("/"+Config.BABEL_DEFAULT_LOCALE+"/")

    @app.route("/favicon.ico", methods=["GET"])
    def serve_favicon_content():
        return send_from_directory("static","favicon.ico")

    @app.context_processor
    def utility_processor():
        from .models import ChatbotFeedbackSurvey,ChatbotFeedbackQuestion
        from app.chatbot.models import FeedbackSurvey

        chatbotsurvey=ChatbotFeedbackSurvey.query.filter_by(active=True).first()
        if chatbotsurvey:
            result=db.session.query(ChatbotFeedbackSurvey, ChatbotFeedbackQuestion).filter(ChatbotFeedbackSurvey.id == ChatbotFeedbackQuestion.survey_id).filter_by(active=True).order_by(ChatbotFeedbackQuestion.order).all()

            feedback_survey=FeedbackSurvey(chatbotsurvey.name,result,len(result))
            return dict(survey=feedback_survey)
        
        return dict(survey=None)

    @login_manager.user_loader
    def load_user(user_id):
        from .models import User
        return User.query.get(user_id)

    return app
