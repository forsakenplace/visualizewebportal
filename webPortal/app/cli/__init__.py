import click
from flask.cli import FlaskGroup
from app import create_app
from app.cli.db import db_cli


@click.group(cls=FlaskGroup, create_app=create_app)
def cli():
    "Management functionality for the portal."""

cli.add_command(db_cli)