import click
from flask.cli import AppGroup
from app import db
from app.models import ChatbotFeedbackSurvey, ChatbotFeedbackQuestion, User, ShinyReport, Highlight


db_cli = AppGroup('db', help='Manage database initialization')

@db_cli.command(name='initdb')
def initialize_db():
    click.echo("Initializing the database...")

    #creates tables in the database
    db.create_all()

    surveys=ChatbotFeedbackSurvey.query.all()
    
    if not surveys:
        click.echo("Initializing the chatbot feedback survey...")
        
        #create a survey
        survey=ChatbotFeedbackSurvey(name="Feedback survey",active=True)
        
        survey.questions.append(ChatbotFeedbackQuestion(text="How did you like your experience on the website?",order=1))
        survey.questions.append(ChatbotFeedbackQuestion(text="What would you like changed?",order=2))
        survey.questions.append(ChatbotFeedbackQuestion(text="Would you rather take vacations instead of answering this survey? If so explain why.",order=3))

        #create questions
        #question1=ChatbotFeedbackQuestion(text="How did you like your experience on the website?")
        #question2=ChatbotFeedbackQuestion(text="What would you like changed?")
        #question3=ChatbotFeedbackQuestion(text="Would you rather take vacations instead of answering this survey? If so explain why.")
        
        #survey.questions.append(question1)
        #survey.questions.append(question2)
        #survey.questions.append(question3)

        #survey2=ChatbotFeedbackSurvey(name="Another survey",active=False)
        
        #create questions
        #question1=ChatbotFeedbackQuestion(text="Did you like the new section of this website? Explain why")
        #question2=ChatbotFeedbackQuestion(text="Do you like the layout of our website?")
        
        #survey2.questions.append(question1)
        #survey2.questions.append(question2)

        db.session.add(survey)
        #db.session.add(survey2)

        db.session.commit()

    #create a user
    users=User.query.all()
    if not users:
        user=User(username="menmarc",password="Password1",display_name="Marc-Andre Menard")
        db.session.add(user)
        db.session.commit()

    #creates the shiny reports
    reports = ShinyReport.query.all()
    if not reports:()
    if not reports:
        report1 = ShinyReport(title_en="Report 1", title_fr="Raport 1", location="example-app")
        report2 = ShinyReport(title_en="Report 2", title_fr="Raport 2", location="example-app2")
        report3 = ShinyReport(title_en="Report 3", title_fr="Raport 3", location="example-app3")
        db.session.add(report1)
        db.session.add(report2)
        db.session.add(report3)
        db.session.commit()

    highlights = Highlight.query.all()
    if not highlights:
        highlight1=Highlight(title_en="International Travel",
        title_fr="Voyage International",
        image="example1.jpg",
        description_en="This bulletin contains advance information, monthly and year-to-date, on visitors entering Canada and on returning Canadian residents. It also includes an estimate of overnight trips by province of entry (visitors) and re-entry (Canadian residents).",
        description_fr="[fr]This bulletin contains advance information, monthly and year-to-date, on visitors entering Canada and on returning Canadian residents. It also includes an estimate of overnight trips by province of entry (visitors) and re-entry (Canadian residents).",
        link="https://www150.statcan.gc.ca/n1/en/catalogue/66-001-P")

        highlight2=Highlight(title_en="Canadian Economic Dashboard",
        title_fr="Tableau de bord économique canadien",
        image="example2.jpg",
        description_en="The Consumer Price Index declined 0.2% on a year-over-year basis in April as lower energy prices pushed the headline rate into negative territory for the first time since September 2009.",
        description_fr="[fr]The Consumer Price Index declined 0.2% on a year-over-year basis in April as lower energy prices pushed the headline rate into negative territory for the first time since September 2009.",
        link="https://www150.statcan.gc.ca/n1/pub/71-607-x/71-607-x2020009-eng.htm")

        highlight3=Highlight(title_en="Greenhouse, sod and nursery industries",
        title_fr="[fr]Greenhouse, sod and nursery industries",
        image="example3.jpg",
        description_en="Total sales from the greenhouse, nursery, field cut flowers and sod industries increased 2.4% from 2018 to $4.0 billion in 2019.",
        description_fr="[fr]Total sales from the greenhouse, nursery, field cut flowers and sod industries increased 2.4% from 2018 to $4.0 billion in 2019.",
        link="https://www150.statcan.gc.ca/n1/daily-quotidien/200506/dq200506a-eng.htm?indid=816-1&indgeo=0")
        
        highlight4=Highlight(title_en="COVID-19 pandemic: What are the impacts?",
        title_fr="[fr]COVID-19 pandemic: What are the impacts?",
        image="example4.jpg",
        description_en="The COVID-19 pandemic has had a number of impacts on the labour market situation of postsecondary students, with one being the cancellation of work placements.",
        description_fr="[fr]The COVID-19 pandemic has had a number of impacts on the labour market situation of postsecondary students, with one being the cancellation of work placements.",
        link="https://www150.statcan.gc.ca/n1/pub/45-28-0001/2020001/article/00022-eng.htm")

        highlight5=Highlight(title_en="StatCan COVID-19: Data to Insights",
        title_fr="[fr]StatCan COVID-19: Data to Insights",
        image="example5.jpg",
        description_en="A series of articles on various subjects which explore the impact of COVID-19 on the socio-economic landscape. New articles will be released periodically.",
        description_fr="[fr]A series of articles on various subjects which explore the impact of COVID-19 on the socio-economic landscape. New articles will be released periodically.",
        link="https://www150.statcan.gc.ca/n1/en/catalogue/45280001")

        db.session.add(highlight1)
        db.session.add(highlight2)
        db.session.add(highlight3)
        db.session.add(highlight4)
        db.session.add(highlight5)

        db.session.commit()

    click.echo("The database is ready")
