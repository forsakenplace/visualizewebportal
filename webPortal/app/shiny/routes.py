from flask import Blueprint, render_template
from flask_babel import gettext
from app import db
from app.models import ShinyReport


shiny = Blueprint("shiny", __name__)


@shiny.route("/rshiny")
def view_rshiny():
    reports = ShinyReport.query.all()
    return render_template("rshiny.html", reports=reports, headerText=gettext("R-shiny feature"))