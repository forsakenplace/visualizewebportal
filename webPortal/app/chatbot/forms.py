from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, IntegerField
from wtforms.validators import DataRequired

class SurveyForm(FlaskForm):
    name = StringField("Name", validators=[DataRequired()])
    submit = SubmitField("Submit")

class QuestionForm(FlaskForm):
    text = StringField("Text", validators=[DataRequired()])
    submit = SubmitField("Submit")

class SurveyCompleteForm(FlaskForm):
    existingQuestionId=IntegerField("existingQuestionId")
    newQuestion=StringField("newQuestion", validators=[DataRequired()])
    surveyId=IntegerField("surveyId", validators=[DataRequired()])