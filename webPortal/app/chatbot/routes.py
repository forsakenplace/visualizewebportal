from flask import Blueprint, render_template, request,redirect,url_for,session, jsonify,make_response
from flask_babel import gettext, get_locale
from flask_login import login_required
from .forms import SurveyForm,QuestionForm,SurveyCompleteForm
from app.models import ChatbotFeedbackSurvey,ChatbotFeedbackQuestion,ChatbotFeedbackResponse
from app import db

import json
import requests


chatbot = Blueprint("chatbot", __name__)

@chatbot.route("/api/getsurveyform")
def get_surveyform():
    response=make_response()
   

    chatbotsurvey=ChatbotFeedbackSurvey.query.filter_by(active=True).first()
    if chatbotsurvey:
        result=db.session.query(ChatbotFeedbackSurvey, ChatbotFeedbackQuestion).filter(ChatbotFeedbackSurvey.id == ChatbotFeedbackQuestion.survey_id).filter_by(active=True).order_by(ChatbotFeedbackQuestion.order).all()
        itemLst=[]
        for item in result:
            itemLst.append(item.ChatbotFeedbackQuestion.serialize_content())

        response=jsonify(items=itemLst)
    
    response=jsonify(items=itemLst)
    response.headers.add("Access-Control-Allow-Origin", "*")
    return response


    return jsonify(items=None)
#@chatbot.route("/chatbot")
#def view_chatbot():
#    return render_template("chatbot.html",headerText=gettext("Chatbot feature"))

@chatbot.route("/dashboard/chatbot/chatbotgraph")
@login_required
def view_chatbot_graph():
    return render_template("chatbot/chatbot_graph.html",headerText=gettext("Chatbot Graph"))

@chatbot.route("/dashboard/chatbot/updatequestionslist",methods=["POST"])
@login_required
def update_questions_list():
    form=SurveyCompleteForm()
    survey=ChatbotFeedbackSurvey.query.get(form.surveyId.data)

    if survey:
        for key in request.form.keys():
            if "btnNewQuestion" in key:
                question=ChatbotFeedbackQuestion()
                question.text=form.newQuestion.data
                question.order=survey.questions.count()+1
                survey.questions.append(question)
                db.session.commit()
                break
            elif "btnDelete" in key:
                questionid=key.replace("btnDelete_","")
                questionToDel=ChatbotFeedbackQuestion.query.get(questionid)
                db.session.delete(questionToDel)
                db.session.commit()
                break

    availableQuestions=getAvailableQuestions(survey.questions)
    results=db.session.query(ChatbotFeedbackSurvey, ChatbotFeedbackQuestion).filter(
        ChatbotFeedbackSurvey.id==form.surveyId.data,ChatbotFeedbackSurvey.id == ChatbotFeedbackQuestion.survey_id).order_by(ChatbotFeedbackQuestion.order).all()

    return render_template("chatbot/manage_questions.html",headerText=gettext("Manage surveys"),survey=survey,questions=results,availableQuestions=availableQuestions)

def getAvailableQuestions(surveyQuestions):
    questions=ChatbotFeedbackQuestion.query.all()

  #TODO this is an horrible loop...outerjoin would be a much better solution 
    availableQuestions = []
    skip=False
    for question in questions:
        for squestion in surveyQuestions:
            if question.id==squestion.id:
                skip=True
                break

        if not skip:
            availableQuestions.append(question)
        skip=False

    return availableQuestions

@chatbot.route("/dashboard/chatbot/updatequestionorder/<int:surveyid>/<int:questionid>/<int:direction>")
@login_required
def update_question_order(surveyid,questionid,direction):
    survey=ChatbotFeedbackSurvey.query.get(surveyid)

    for row in survey.questions:
        if row.id==questionid:
            #find the question that has the target order no
            for trow in survey.questions:
                if trow.order==direction:
                    trow.order=row.order
                    row.order=direction
                    db.session.commit()
                    break
            break

    results=db.session.query(ChatbotFeedbackSurvey, ChatbotFeedbackQuestion).filter(
        ChatbotFeedbackSurvey.id == ChatbotFeedbackQuestion.survey_id,ChatbotFeedbackSurvey.id==surveyid).order_by(ChatbotFeedbackQuestion.order).all()

    availableQuestions=getAvailableQuestions(survey.questions)

    return render_template("chatbot/manage_questions.html",headerText=gettext("Manage surveys"),survey=survey,questions=results,availableQuestions=availableQuestions)

@chatbot.route("/dashboard/chatbot/managequestions/<int:surveyid>")
@login_required
def manage_questions(surveyid):

    survey=ChatbotFeedbackSurvey.query.get(surveyid)
    availableQuestions=getAvailableQuestions(survey.questions)

    results=db.session.query(ChatbotFeedbackSurvey, ChatbotFeedbackQuestion).filter(
        ChatbotFeedbackSurvey.id == ChatbotFeedbackQuestion.survey_id,ChatbotFeedbackSurvey.id==surveyid).order_by(ChatbotFeedbackQuestion.order).all()

    return render_template("chatbot/manage_questions.html",headerText=gettext("Manage surveys"),survey=survey,questions=results,availableQuestions=availableQuestions)

@chatbot.route("/dashboard/chatbot/managesurveys")
@login_required
def manage_surveys():
    surveys=ChatbotFeedbackSurvey.query.all()
    return render_template("chatbot/manage_surveys.html",headerText=gettext("Manage surveys"),surveys=surveys)

#TODO not secure at all
@chatbot.route("/dashboard/chatbot/activatesurvey/<int:id>",methods=["GET"])
@login_required
def activate_survey(id):
    survey=ChatbotFeedbackSurvey.query.filter_by(active=True).first()
    if survey:
        survey.active=False

    surveyToActivate=ChatbotFeedbackSurvey.query.get(id)
    surveyToActivate.active=True

    db.session.commit()
    return redirect(url_for("chatbot.manage_surveys"))
     
@chatbot.route("/dashboard/chatbot/addsurvey",methods=["GET","POST"])
@login_required
def add_survey():

    form=SurveyForm()
    if form.validate_on_submit():
        s = ChatbotFeedbackSurvey(name=form.name.data, active=False)
        db.session.add(s)
        db.session.commit()
        return redirect(url_for("chatbot.manage_surveys"))

    return render_template("chatbot/survey_form.html",headerText=gettext("Add a new survey"),form=form)

@chatbot.route("/dashboard/chatbot/deletesurvey",methods=["POST"])
@login_required
def delete_survey():

    for key in request.form.keys():
        if "delete" in key:
            surveyid=key.replace("delete_","")
            survey=ChatbotFeedbackSurvey.query.filter_by(id=surveyid).first()
            db.session.delete(survey)
            db.session.commit()

    return redirect(url_for('chatbot.manage_surveys'))

@chatbot.route("/dashboard/chatbot/updatesurvey/<int:id>", methods=["GET","POST"])
@login_required
def update_survey(id):
    
    survey = ChatbotFeedbackSurvey.query.get_or_404(id)
 
    form = SurveyForm()
    if form.validate_on_submit():
        survey.name = form.name.data
        db.session.commit()
        return redirect(url_for("chatbot.manage_surveys"))
    
    form.name.data=survey.name
    return render_template("chatbot/survey_form.html",headerText=gettext("Update survey"),form=form)

@chatbot.route("/dashboard/chatbot/updatequestion/<int:id>", methods=["GET","POST"])
@login_required
def update_question(id):
    question = ChatbotFeedbackQuestion.query.get_or_404(id)
    
    form = QuestionForm()
    if form.validate_on_submit():
        question.text = form.text.data
        db.session.commit()
        return redirect(url_for("chatbot.manage_surveys"))
    
    form.text=question.text
    return render_template("chatbot/manage_question.html",headerText=gettext("Update question"),form=form)

@chatbot.route("/dashboard/chatbot/managesurveyresponses/<int:surveyid>", methods=["GET"])
@login_required
def manage_survey_responses(surveyid):
    survey=ChatbotFeedbackSurvey.query.get_or_404(surveyid)
    return render_template("chatbot/manage_survey_responses.html",headerText=gettext("Manage responses"),survey=survey)

@chatbot.route("/dashboard/chatbot/viewsurveyresponse/<int:responseid>", methods=["GET"])
@login_required
def get_survey_response(responseid):
    r=ChatbotFeedbackResponse.query.get_or_404(responseid)
    response = make_response(r.response, 200)
    response.mimetype = "text/plain"
    return response

@chatbot.route("/chat")
def chat_chatbot():
    user_message = request.args["message"]
    lang = get_locale().language
    try:
        response = requests.get("http://chatbot:5005/"+lang+"/question/"+user_message)
        return response.text
    except:
        return gettext("Sorry, I seem to be asleep right now. Please try again later")

@chatbot.route("/questions")
def get_chatbot_questions():
    lang = get_locale().language
    try:
        response = requests.get("http://chatbot:5005/"+lang+"/questions")
        return response.text
    except:
        return gettext("Sorry, I seem to be asleep right now. Please try again later")

@chatbot.route("/delete_message", methods=["POST"])
def delete_chatbot_message():
    user_message = request.form["message_id"]
    try:
        response = requests.post("http://chatbot:5005/deleteChatMessage", data={"message_id" : user_message})
        return response.text, response.status_code
    except:
        return gettext("Sorry, I seem to be asleep right now. Please try again later")

@chatbot.route("/add_message", methods=["POST"])
def add_chatbot_message():
    question_message = request.form["question_id"]
    new_message_en = request.form["add_text_en"]
    new_message_fr = request.form["add_text_fr"]
    try:
        response = requests.post("http://chatbot:5005/addChatMessage", data={"question_id" : question_message, "new_message_en" : new_message_en, "new_message_fr" : new_message_fr})
        return response.text, response.status_code
    except:
        return gettext("Sorry, I seem to be asleep right now. Please try again later")


@chatbot.route("/chatbot/submitfeedbacksurvey", methods=["POST"])
def submit_feedback_survey():
    #get the survey id

    survey=ChatbotFeedbackSurvey.query.filter_by(active=True).first()

    #create a new response
    response=ChatbotFeedbackResponse()
    response.response=json.dumps(request.form)
    survey.responses.append(response)
    db.session.commit()

    return response.response,200