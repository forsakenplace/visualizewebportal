class FeedbackSurvey():
    def __init__(self,survey_name,questions,item_count):
        self.__item_count = item_count
        self.__questions=questions
        self.__survey_name=survey_name

    @property
    def item_count(self):
        return self.__item_count

    @property
    def questions(self):
        return self.__questions

    @property
    def survey_name(self):
        return self.__survey_name