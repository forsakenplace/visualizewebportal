import bcrypt

from flask_login import UserMixin
from flask_babel import get_locale
from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy import ForeignKey
from app import db

class User(UserMixin, db.Model):
    __tablename__ = 'User'
    # The explicit autoincrement prevents ID reuse on sqlite.
    #: Unique identifier for this user.
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)

    #: ASCII-safe username for this user.
    username = db.Column(db.String(255), unique=True, nullable=False)
    _password = db.Column(db.LargeBinary(255 + 64), nullable=False)

    #: Optional display name (use this for "real name").
    display_name = db.Column(db.String(255))

    @hybrid_property
    def password(self):

        return self._password

    @password.setter
    def password(self, text: str):

        # scrypt is available in Py3.6+, except it requires a newer version of
        # OpenSSL (1.1+) then is available on the OS X binary installers.
        # Should migrate when possible.
        self._password = bcrypt.hashpw(
            text.encode('utf-8'),
            bcrypt.gensalt(14)
        )

    def check_password(self, text) -> bool:

        return bcrypt.checkpw(text.encode('utf-8'), self._password)

    def get_id(self) -> str:
        return str(self.id)
        
class Entry(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    text = db.Column(db.String(100))

    def __repr__(self):
        return "Entry({0}, {1})".format(self.id, self.text)

class ShinyReport(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title_en = db.Column(db.String(100))
    title_fr = db.Column(db.String(100))
    location = db.Column(db.String(100))

    @property
    def title(self):
        if get_locale().language == "fr":
            return self.title_fr
        else:
            return self.title_en

    def __repr__(self):
        return "ShinyReport({0}, {1}, {2}, {3})".format(self.id, self.title_en, self.title_fr, self.location)

class Highlight(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title_en = db.Column(db.String(50))
    title_fr = db.Column(db.String(50))
    image = db.Column(db.String(50), default="example1.jpg")
    description_en = db.Column(db.String(400))
    description_fr = db.Column(db.String(400))
    link = db.Column(db.String(200))

    @property
    def title(self):
        if get_locale().language == "fr":
            return self.title_fr
        else:
            return self.title_en

    @property
    def description(self):
        if get_locale().language == "fr":
            return self.description_fr
        else:
            return self.description_en

    def __repr__(self):
        return "Highlight({0}, {1}, {2}, {3}, {4}, {5}, {6})".format(self.id, self.title_en, self.title_fr, self.image, self.description_en, self.description_fr, self.link)

class ChatbotFeedbackQuestion(db.Model):
    __tablename__ = 'ChatbotFeedbackQuestion'
    id = db.Column(db.Integer, primary_key=True)
    text=db.Column(db.String(200))
    survey_id = db.Column(db.Integer,db.ForeignKey('ChatbotFeedbackSurvey.id'),nullable=False)
    order = db.Column(db.Integer, default=0)
    def serialize_content(self):
        return {
            "id":self.id,
            "question_text":self.text,
            "order":self.order
        }

class ChatbotFeedbackSurvey(db.Model):
    __tablename__ = 'ChatbotFeedbackSurvey'
    id = db.Column(db.Integer, primary_key=True)
    name=db.Column(db.String(50))
    active=db.Column(db.Boolean, default=False)
    questions = db.relationship("ChatbotFeedbackQuestion", lazy='dynamic', cascade="all,save-update, delete-orphan,delete")
    responses=db.relationship('ChatbotFeedbackResponse', backref='ChatbotFeedbackSurvey', lazy=True,cascade="all, delete-orphan,delete")
#back_populates="survey",
class ChatbotFeedbackResponse(db.Model):
    __tablename__ = 'ChatbotFeedbackResponse'
    id = db.Column(db.Integer, primary_key=True)
    survey_id = db.Column(db.Integer, db.ForeignKey('ChatbotFeedbackSurvey.id'),nullable=False)
    response=db.Column(db.Text)