from flask import Blueprint, render_template, redirect, url_for
from flask_babel import gettext
from app.models import Entry
from app import db
from app.records.forms import EntryForm

records = Blueprint("records", __name__)


@records.route("/view_records")
def view_records():
    #gets the data from the table
    entries = Entry.query.all()
    return render_template("view_records.html", entries=entries,headerText=gettext("View records"))

@records.route("/new_entry", methods=["GET", "POST"])
def new_entry():
    form = EntryForm()
    if form.validate_on_submit():
        entry = Entry(text=form.text.data)
        db.session.add(entry)
        db.session.commit()
        return redirect(url_for("records.view_records"))
    return render_template("new_entry.html", form=form, headerText=gettext("Add new entry"))

@records.route("/update/<int:entry_id>", methods=["GET", "POST"])
def update_post(entry_id):
    entry = Entry.query.get_or_404(entry_id)
    
    form = EntryForm()
    if form.validate_on_submit():
        entry.text = form.text.data
        db.session.commit()
        return redirect(url_for("records.view_records"))
    
    form.text.data = entry.text
    return render_template("new_entry.html", form=form, headerText=gettext("Update Entry"))

@records.route("/delete/<int:entry_id>", methods=["POST"])
def delete_post(entry_id):
    entry = Entry.query.get_or_404(entry_id)
    
    db.session.delete(entry)
    db.session.commit()
    
    return redirect(url_for("records.view_records"))