from flask import Blueprint, render_template, redirect, url_for, current_app
from flask_babel import gettext
from app.models import Highlight
from app.highlights.forms import HightlightForm 
import os, secrets
from PIL import Image
from app import db
from flask_login import login_required

highlights = Blueprint("highlights", __name__)


@highlights.route("/dashboard/highlights")
@login_required
def view_highlights():
    highlights = Highlight.query.all()
    return render_template("highlights.html",headerText=gettext("Highlights"), highlights=highlights)


def save_image(form_image):
    random_hex = secrets.token_hex(8)
    _, f_ext = os.path.splitext(form_image.filename)
    image_filename = random_hex+f_ext
    image_path = os.path.join(current_app.root_path, "static/media/highlight_images", image_filename)

    output_size = (300, 300)
    i = Image.open(form_image)
    i.thumbnail(output_size)
    i.save(image_path)
    
    return image_filename


@highlights.route("/dashboard/new_highlight", methods=["GET", "POST"])
@login_required
def new_highlight():
    form = HightlightForm()
    if form.validate_on_submit():
        h = Highlight(title_en=form.title.data, description_en=form.description.data, link=form.link.data)
        if(form.image.data):
            image_filename = save_image(form.image.data)
            h.image = image_filename
        db.session.add(h)
        db.session.commit()
        return redirect(url_for("highlights.view_highlights"))

    return render_template("highlights_form.html", headerText = gettext("Add a new Highlight"), form=form)



@highlights.route("/dashboard/update_highlight/<int:highlight_id>", methods=["GET", "POST"])
@login_required
def update_highlight(highlight_id):
    highlight = Highlight.query.get_or_404(highlight_id)
    
    form = HightlightForm()
    if form.validate_on_submit():
        highlight.title_en = form.title.data
        highlight.description_en = form.description.data
        highlight.link = form.link.data
        if(form.image.data):
            image_filename = save_image(form.image.data)
            highlight.image = image_filename
        db.session.commit()
        return redirect(url_for("highlights.view_highlights"))
    
    form.title.data = highlight.title_en
    form.description.data = highlight.description_en
    form.link.data = highlight.link
    return render_template("highlights_form.html", headerText = gettext("Update the Highlight"), form=form)


@highlights.route("/dashboard/delete_highlight/<int:highlight_id>", methods=["POST"])
@login_required
def delete_highlight(highlight_id):
    highlight = Highlight.query.get_or_404(highlight_id)

    db.session.delete(highlight)
    db.session.commit()
    
    return redirect(url_for("highlights.view_highlights"))