from flask_wtf import FlaskForm
from flask_wtf.file import FileField, FileAllowed
from wtforms import StringField, SubmitField
from wtforms.validators import DataRequired

class HightlightForm(FlaskForm):
    title = StringField("title", validators=[DataRequired()])
    image = FileField("Image", validators=[FileAllowed(["png", "jpg"])])
    description = StringField("description", validators=[DataRequired()])
    link = StringField("link", validators=[DataRequired()])
    submit = SubmitField("Submit")
