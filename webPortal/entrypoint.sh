#!/bin/bash
set -e

while !</dev/tcp/db/5432; do
    echo "Waiting for POSTGRES"
    sleep 1
done

python setup.py develop

#flask run --host=0.0.0.0
webPortal db initdb

webPortal run --host=0.0.0.0