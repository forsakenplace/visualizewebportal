# Visualize Web Portal

This project was made with the intention of creating a repository where we can experiment with different technologies that we will possibly need to know later on.

## Getting Started

### Prerequisites

To work with this project, users will need a linux machine with docker and docker composed installed. This is a python 3 project using the flask framework, so users will also need the required tools to make those work. Once the linux machine is open, run the following commands to install the prerequisites :
```
$ sudo apt-get update

$ sudo apt-get install docker.io

$ sudo apt install curl
$ sudo curl -L "https://github.com/docker/compose/releases/download/1.24.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
$ sudo chmod +x /usr/local/bin/docker-compose

$ sudo apt install python3
$ sudo apt install python3-pip

$ pip3 install flask
```

There might be a need for an initial command that needs to be run before the application can be started. Therefore, it might be necessary to navigate to the project repository and run the following commands : 
```
$ chmod +x webPortal/entrypoint.sh
$ chmod +x chatbot/entrypoint.sh

```

Then, to run the application, all that should be needed is to run the following command : 
```
$ sudo docker-compose up
```

To close the application, just do CTRL+C in the terminal.

Right now, the application is really bare-bones, but we will keep adding new stuff as we go along. If something doesn't work as intended or is missing, feel free to fix it or let someone know!

## Repository content

### Web portal
The web portal is located under folder "webPortal". It will run under http://localhost:5000

### Web portal (react frontend)
The web portal using a react front end is located under folder "webPortalReact". It will run under http://localhost:5002.
React components are under app/react/src folder.

If you want to modify react components and see your changes as you develop, you need to access folder /app/react and execute "npm start". That will start parcel and will track modifications done to app/react/src/index.js (add all files linked to it). It compiles the whole application in the app/static folder. The index.html file (under "templates" folder) references the compiled js file located in static folder.

The react application do a web server call ("getData" which is defined in routes.py) that returns a json response which is displayed by the react application. 

### Shiny server
The shiny server is located under folder "shiny". This is the same image used by project 22 (https://github.com/StatCan/shiny).The shiny server can be accessed on http://localhost:3838.

A basic shiny report can be found under http://localhost:3838/example-app/ You can add shiny reports  by creating a folder for each new report under "moutpoints/apps". Note that it takes around 10 minutes to build the initial image.

## Localization
We use Flask-Babel to handle the translation (https://pythonhosted.org/Flask-Babel/). First, install it with :
```
$ sudo pip3 install Flask-Babel
```

Afterwards, whenever you will have text that needs to be translated, you will need to mark them with the gettext() function. In the html files, you can also use _(). This looks like :
```
python_text = gettext("Hello")
```
or
```
<h1>{{ _("Heading Text") }}</h1>
<h2>{{ gettext("More Text") }}</h2>
```

Once the messages are all indicated, some commands need to be ran to set up the translations.
First, we need to create a POT file. This will house a reference to all our messages and serve as a template for the translation files. 
So navigate to the webPortal folder and run the following: 
```
$ pybabel extract -F babel.cfg -o messages.pot .
```

Next, if the language you want to accomodate isn't in the translations folder, or if you don't see a translation folder, run this command to initialize that language. This will create a PO file based on the messages.pot file that we created earlier for the language code that is specified at the end (in our case, canadian french).
```
$ pybabel init -i messages.pot -d translations -l fr_CA
```

Once that is done, you will see the messages.po file we just created under "translations/fr_CA/LC_MESSAGES/messages.po". That is the file that you need to update to specify the translated value for each message. Once all the edits are done, the file needs to be compiled with the command :
```
$ pybabel compile -d translations
```

There you go! But what if you need to create new messages? You will first need to recreate the POT file we made using the same command that we used in the first step. Afterwards, run the following to update the PO files with the new messages while also keeping what was already translated:
```
$ pybabel update -i messages.pot -d translations
```

## Testing
To be able to run the unit tests, the packages need to be installed first. To do so, run the following :
```
$ sudo pip3 install pytest
$ sudo pip3 install pytest-docker
```

Then all that should be required to run the test is to run :
```
$ pytest
```
That command should then initiate all the tests that are declared in files following the name "test_*.py". Since we are using docker compose to bring up the application, the tests will also first build the application with the same docker-compose.yml file.