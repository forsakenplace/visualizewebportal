from flask import Flask
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)
app.config["SQLALCHEMY_DATABASE_URI"] = "postgresql://testuser:testpassword@db/testdb"
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = "False"
db = SQLAlchemy(app)

from chatbot_service.routes import chatbot
app.register_blueprint(chatbot)

from chatbot_service.cli import chatbot_cli
app.cli.add_command(chatbot_cli)
