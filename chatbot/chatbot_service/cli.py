from chatbot_service import db
from flask.cli import AppGroup
from chatbot_service.models import ChatMessage,   ChatMessageAnswer
import click

chatbot_cli = AppGroup("chatbot")

@chatbot_cli.command("initdb")
def initdb():
    click.echo("Initializing the database...")

    #creates tables in the database
    db.create_all()

    #creates the shiny reports
    data = ChatMessage.query.all()
    if not data:
        db.session.add(ChatMessage(message_en="Hello! How may I help you?", message_fr="Bonjour! Comment puis-je vous aider?", greet_message=True))

        db.session.add(ChatMessage(message_en="What is DAaaS?", message_fr="Qu'est-ce qu'est DAaaS?", user_response=True))
        db.session.add(ChatMessage(message_en="Data Analytics as a service (DAaaS) is a modern, Cloud-based platform which enables the delivery of business capabilities in a more agile, responsive manner. This video shows what DAaaS system is : https://www.youtube.com/watch?v=AsSJQ0JBJoQ", message_fr="L'Analyse de données en tant que service (ADS) est une plateforme moderne basée sur l'infonuagique qui permet la livraison de capabilités commerciales d'une façon plus agile et sensible. Cette vidéo démontre ce qu'est un système ADS : https://www.youtube.com/watch?v=AsSJQ0JBJoQ"))
        db.session.add(ChatMessage(message_en="What are the key DAaaS components?", message_fr="Quelles sont les composantes clées du ADS?", user_response=True))
        db.session.add(ChatMessage(message_en="Key components include a data management hub, data ingestion services, data analysis using artificial intelligence, machine learning techniques and high-performance data processing.", message_fr="Les composantes clées incluent un centre de gestion de données, des services d'ingestion de données, de l'analyse de données utilisant de l'intelligence artificielle, des techniques de machine learning et du traitement de données à haute performance."))
        db.session.add(ChatMessage(message_en="What are the technologies used?", message_fr="Quelles sont les technologies utilisées?", user_response=True))
        db.session.add(ChatMessage(message_en="DAaaS uses open source software, Cloud computing Kubernetes, containerization, Hadoop, Spark, Jupyter Notebook, JupyterHub, RESTFUL API, SOLR, Elastic Search, etc.", message_fr="ADS utilise des logiciels open source, l'infonuagique, Kubernetes, conteneurisation, Hadoop, Spark, Jupyter Notebook, JupyterHub, RESTFUL API, SOLR, Elastic Search, etc."))
        db.session.add(ChatMessage(message_en="Which tools are available?", message_fr="Quels sont les outils disponibles?", user_response=True))
        db.session.add(ChatMessage(message_en="The analytical and visualization tools currently in use are Jupyter notebooks (R, Python) and PowerBI. More tools will become available with time.", message_fr="Les outils d'analyses et de visualization qui sont présentement utilisés sont Jupyter notebooks (R, python) et PowerBI. Plus d'outils deviendront disponible avec le temps."))
        db.session.add(ChatMessage(message_en="Who are the DAaaS users?", message_fr="Qui sont les utilisateurs du ADS?", user_response=True))
        db.session.add(ChatMessage(message_en="DAaaS users are external people (researchers, policy analyst, data scientists, politicians, journalists, citizen data analysts, etc.) as well as internal employees.", message_fr="Les utilisateurs d'ADS sont des personnes externes (rechercheurs, analystes de politiques, scientifiques de données, politiciens, journalistes, analystes de données de citoyens, etc.) avec des employés internes."))
        db.session.add(ChatMessage(message_en="Which users get access to DAaaS", message_fr="Quels utilisateurs ont accès au ADS?", user_response=True))
        db.session.add(ChatMessage(message_en="Onboarded users get a space to work, powerful computing, ability to share and a place to publish results.", message_fr="Les utilisateurs intégrés obtiennent un espace de travail, puissante informatique, abilité de partage et un endroit pour publier des résultats."))
        db.session.flush()

        db.session.add(ChatMessageAnswer(question_id=1, answer_id=2))
        db.session.add(ChatMessageAnswer(question_id=2, answer_id=3))
        db.session.add(ChatMessageAnswer(question_id=1, answer_id=4))
        db.session.add(ChatMessageAnswer(question_id=4, answer_id=5))
        db.session.add(ChatMessageAnswer(question_id=1, answer_id=6))
        db.session.add(ChatMessageAnswer(question_id=6, answer_id=7))
        db.session.add(ChatMessageAnswer(question_id=1, answer_id=8))
        db.session.add(ChatMessageAnswer(question_id=8, answer_id=9))
        db.session.add(ChatMessageAnswer(question_id=1, answer_id=10))
        db.session.add(ChatMessageAnswer(question_id=10, answer_id=11))
        db.session.add(ChatMessageAnswer(question_id=1, answer_id=12))
        db.session.add(ChatMessageAnswer(question_id=12, answer_id=13))
        db.session.commit()

    click.echo("Database ready!")