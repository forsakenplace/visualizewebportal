from flask import Blueprint, request, jsonify,make_response
from chatbot_service import db
from chatbot_service.models import ChatMessage, ChatMessageAnswer, ChatbotItem

chatbot = Blueprint("chatbot", __name__)

@chatbot.route("/")
def test():
    return "Service is running"

def GenerateTree(currentItemId,subItems,ignore,questionsList):

    #step 1 : add the parent and parent's choice to our list
    if (subItems.count()>1 or (subItems.count()==1 and ChatMessage.query.get(subItems[0].answer_id).user_response)):
        childs=[]
        for child in subItems:
            #check for their trigger value
            results=ChatMessageAnswer.query.filter(ChatMessageAnswer.question_id==child.answer_id)
            triggerName=''
            if(results.count()>1):
                triggerName="option_" + str(child.answer_id)
            elif (results.count()==1):
                triggerName=results[0].answer_id

            childs.append(ChatbotItem(value="answer_" + str(child.answer_id),label=ChatMessage.query.get(child.answer_id).message_en,trigger=str(triggerName)))
        
        questionsList.append(ChatbotItem(id=str(currentItemId),message=ChatMessage.query.get(subItems[0].question_id).message_en,trigger="option_" + str(currentItemId)))
        questionsList.append(ChatbotItem(id="option_" + str(currentItemId),options=childs))
    elif (subItems.count()==1):
        if (not ignore):
            questionsList.append(ChatbotItem(id=str(currentItemId),message=ChatMessage.query.get(subItems[0].question_id).message_en,trigger=str(subItems[0].answer_id)))
    else:
        #if it is the last item we loop back at the beginning
        questionsList.append(ChatbotItem(id=str(currentItemId),message=ChatMessage.query.get(currentItemId).message_en,trigger='option_beginning'))

    #for each child
    for rec in subItems:

        #check if they have children
        nextRec=ChatMessageAnswer.query.filter(ChatMessageAnswer.question_id == rec.answer_id)
        GenerateTree(rec.answer_id,nextRec, (subItems.count() >1 or (subItems.count()==1 and ChatMessage.query.get(subItems[0].answer_id).user_response)) ,questionsList) 
       

@chatbot.route("/<lang_code>/questionreact")
def question_call_react(lang_code):

    questions = []
    rootQuestion = ChatMessage.query.filter(ChatMessage.id == 1).first()
    
    #get all childs of root node
    childs = ChatMessageAnswer.query.filter(ChatMessageAnswer.question_id == rootQuestion.id)
    #generate all nodes
    GenerateTree(rootQuestion.id,childs,False,questions)

    response=make_response()
    response = jsonify(items=[q for q in questions])
    response.headers.add("Access-Control-Allow-Origin", "*")
    return response

@chatbot.route("/<lang_code>/question/<int:question_id>")
def question_call(lang_code, question_id):
    asked_question = ChatMessage.query.filter(ChatMessage.id == question_id).first()
    if asked_question.greet_message:
        answer = asked_question
    else:
        question_answer_link = ChatMessageAnswer.query.filter(ChatMessageAnswer.question_id == asked_question.id).first()
        answer = ChatMessage.query.filter(ChatMessage.id == question_answer_link.answer_id).first()

    message_links = ChatMessageAnswer.query.filter(ChatMessageAnswer.question_id == answer.id).all()
    questions = []
    for link in message_links:
        questions.append(ChatMessage.query.filter(ChatMessage.id == link.answer_id).first())
    
    if lang_code == "fr":
        return jsonify(answer = answer.serialize_fr(), questions = [question.serialize_fr() for question in questions])
    else:
        return jsonify(answer = answer.serialize_en(), questions = [question.serialize_en() for question in questions])

@chatbot.route("/<lang_code>/questions")
def get_questions(lang_code):
    questions = ChatMessage.query.all()
    question_answers = ChatMessageAnswer.query.all()
    if lang_code == "fr":
        return jsonify(questions = [question.serialize_fr() for question in questions], answers = [answer.serialize() for answer in question_answers])
    else:
        return jsonify(questions = [question.serialize_en() for question in questions], answers = [answer.serialize() for answer in question_answers])

@chatbot.route("/deleteChatMessage", methods=["POST"])
def delete_chatMessage():
    try:
        message_id = request.form["message_id"]
        message = ChatMessage.query.filter(ChatMessage.id == message_id).first()
        if message:
            answers_from = ChatMessageAnswer.query.filter(ChatMessageAnswer.question_id == message_id).all()
            answers_to = ChatMessageAnswer.query.filter(ChatMessageAnswer.answer_id == message_id).all()

            for a in answers_from:
                db.session.delete(a)
            for a in answers_to:
                db.session.delete(a)

            db.session.delete(message)
            db.session.commit()
            return "Message deleted sucessfully"
        else:
            raise Exception()
    except:
        return "An error has occured and the message was not deleted"

@chatbot.route("/addChatMessage", methods=["POST"])
def add_chatMessage():
    try:
        message_id = request.form["question_id"]
        new_message_en = request.form["new_message_en"]
        new_message_fr = request.form["new_message_fr"]

        message = ChatMessage.query.filter(ChatMessage.id == message_id).first()

        if message:
            new_chat = ChatMessage(message_en=new_message_en, message_fr=new_message_fr)
            if(not message.user_response):
                new_chat.user_response = True
            db.session.add(new_chat)
            db.session.flush()

            db.session.add(ChatMessageAnswer(question_id=message.id, answer_id=new_chat.id))
            db.session.commit()
            return str(new_chat.id)
        else:
            raise Exception()
    except:
        return "An error has occured and the message was not added"