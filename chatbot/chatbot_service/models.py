from chatbot_service import db

class ChatbotItem(dict):
    def __init__(self, id=0, label='',value='', message='',trigger='',options=[]):
        if (len(options) > 0):
            dict.__init__(self,id=id,options=options)
        elif (len(label)>0):
            dict.__init__(self,value=value,label=label,trigger=trigger)
        else:
            dict.__init__(self,id=id,message=message,trigger=trigger)

class ChatMessage(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    message_en = db.Column(db.String(300))
    message_fr = db.Column(db.String(300))
    user_response = db.Column(db.Boolean)
    greet_message = db.Column(db.Boolean)

    def serialize_en(self):
        return {
            "id":self.id,
            "message":self.message_en,
            "user_response":self.user_response,
            "greet_message":self.greet_message
        }

    def serialize_fr(self):
        return {
            "id":self.id,
            "message":self.message_fr,
            "user_response":self.user_response,
            "greet_message":self.greet_message
        }

    def __repr__(self):
        return "ChatMessage({0}, {1}, {2}, {3}, {4})".format(self.id, self.message_en, self.message_fr, self.user_response, self.greet_message)


class ChatMessageAnswer(db.Model):
    question_id = db.Column(db.Integer, db.ForeignKey("chat_message.id"), primary_key=True)
    answer_id = db.Column(db.Integer, db.ForeignKey("chat_message.id"), primary_key=True)

    def serialize(self):
        return {
            "question_id":self.question_id,
            "answer_id":self.answer_id
        }

    def __repr__(self):
        return "ChatMessageAnswer({0}, {1})".format(self.question_id, self.answer_id)
