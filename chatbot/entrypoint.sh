#!/bin/bash
set -e

while !</dev/tcp/db/5432; do
    echo "Waiting for POSTGRES"
    sleep 1
done

flask chatbot initdb

flask run --host=0.0.0.0 --port=5005