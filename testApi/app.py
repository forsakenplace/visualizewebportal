from flask import Flask, render_template

app = Flask(__name__)

@app.route("/")
def home():
    return "This message is from another service"

if __name__ == "__main__":
    app.run(debug=True, host="0.0.0.0", port=5001)